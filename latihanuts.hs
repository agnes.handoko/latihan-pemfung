-- 1. Rancanglah, higher order function yang bekerja pada struktur data ekspresi dan memiliki
-- semantik seperti: Fungsi map pada list, Fungsi fold pada tree
fungsiMapFold xs = foldl (+) 0 (map (+1) xs)
fungsiMapFold2 xs = foldl (+) 0 (map (\x -> x+1) xs)

data Tree a = Leaf a | Branch (Tree a) (Tree a)

mapTree :: (a->b) -> Tree a -> Tree b

mapTree f (Leaf x)       = Leaf (f x)
mapTree f (Branch t1 t2) = Branch (mapTree f t1) 
                                   (mapTree f t2)

foldtl f z (Leaf x)       = f z x
foldtl f z (Branch t1 t2) = foldtl f (f z t1) t2

foldtr f z (Leaf x)       = f z x
foldtr f z (Branch t1 t2) = f t1 (foldtr f z t2)

-- 2. Pada buku dan contoh sebelumnya, fungsi evaluasi didefinisikan secara rekursif. Gunakan
-- higher order function yang baru dibuat untuk mendefinisikan fungsi evaluasi tersebut.
-- Pastikan memiliki makna semantik yang sama dengan definisi sebelumnya.
data Expr = C Float 
        | Expr :+ Expr 
        | Expr :- Expr
        | Expr :* Expr 
        | Expr :/ Expr 
        | V [Char]
        | Let String Expr Expr      
        deriving Show

> subst :: String -> Expr -> Expr -> Expr

> subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
> subst v0 e0 (C c)          = (C c)
> subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

> evaluate :: Expr -> Float
> evaluate (C x) = x
> evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
> evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
> evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
> evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2
> evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
> evaluate (V v)         = 0.0   


data Expr = C Float 
        | Expr :+ Expr 
        | Expr :- Expr
        | Expr :* Expr 
        | Expr :/ Expr
        | V String
        | Let String Expr Expr
    deriving Show

add :: Expr -> Expr -> Expr    
add (C c) (C d) = C (c+d)

sub :: Expr -> Expr -> Expr    
sub (C c) (C d) = C (c-d)

mul :: Expr -> Expr -> Expr    
mul (C c) (C d) = C (c*d)

divide :: Expr -> Expr -> Expr    
divide (C c) (C d) = C (c/d)

foldArithmetic (a,s,m,d) (C c) = c
foldArithmetic (a,s,m,d) (e1 :+ e2 ) = a (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)
foldArithmetic (a,s,m,d) (e1 :- e2 ) = s (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)
foldArithmetic (a,s,m,d) (e1 :* e2 ) = m (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)
foldArithmetic (a,s,m,d) (e1 :/ e2 ) = d (foldArithmetic (a,s,m,d) e1) (foldArithmetic (a,s,m,d) e2)

-- newEvaluate expr = foldArithmetic (aa, as, am, ad) expr
newEvaluate = foldArithmetic (aa, as, am, ad)
    where aa = (+)
          as = (-)
          am = (*)
          ad = (/)



-- 1. Apakah kita bisa menambahkan pada RobotState informasi tentang Energy? Misalnya robot
-- hanya bisa bergerak bisa masih ada energi-nya dan energinya akan terpakai. Versi awal
-- setiap gerakan mengkonsumsi energi yang sama, versi selanjutnya tiap gerakan memiliki
-- kebutuhan energi nya masing-masing termasuk penDown misalnya. Fungsi tambahan apa
-- lagi yang dibutuhkan?


-- 2. Pelajari implementasi dari moven. Apakah tipe dari fungsi mapM_ ? apa makna dari fungsi
-- tersebut?
-- :t mapM_
mapM :: (Traversable t, Monad m) => (a -> m b) -> t a -> m (t b)

-- 3. Perhatikan implementasi fungsi spiral yang dicontohkan. Apakah bisa dibuatkan perintah
-- baru forloop misalnya sehingga programmer robot bisa mengimplementasikan fungsi
-- spiral yang sama dengan cara yang lebih mudah misalnya seperti berikut ini saja:

-- spiral movement using for forloop
> spiral = forLoop [1..20] action
> where action = \i -> (turnRight >> moven i >> turnRight >> moven i)
-- zigzag with for loop
> zigzag = forLoop [1..10] action
> where action = \i -> (moven 5 >> turnRight >> move >> turnRight
> >> moven 5 >> turnLeft >> move >> turnLeft