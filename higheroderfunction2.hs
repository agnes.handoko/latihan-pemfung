-- II. List Comprehensions and Higher-Order Functions
import System.IO
-- 1. Can you rewrite the following list comprehensions using the higher-order functions map and filter?
-- You might need the function concat too
-- a. [ x+1 | x <- xs ]
addOne :: [Int] -> [Int]
addOne xs = map (\x -> x+1) xs 
addOne2 xs = map (+1) xs
-- addOne [1,2,3] output [2,3,4]

-- b. [ x+y | x <- xs, y <-ys ]
sumElem :: [Int] -> [Int] -> [Int]
sumElem xs ys = concat (map (\x -> map (\y -> x+y) ys) xs)
-- sumElem [1,2,3] [4,5,6] output [5,7,9]
-- [fx1, fx2, fx3]
-- [(x1,4),(x2,5),(x3,6)]
-- [(1,4),(2,5),(3,6)]
-- [5,7,9]

-- c. [ x+2 | x <- xs, x > 3 ]
addTwoIfBiggerThanThree :: [Int] -> [Int]
addTwoIfBiggerThanThree xs = map (\x -> x+2) (filter (>3) xs)
addTwoIfBiggerThanThree2 xs = map (+2) (filter (> 3) xs)
-- addTwoIfBiggerThanThree [1,2,3,4,5] output [6,7]
-- addTwoIfBiggerThanThree [1,2,3] outputnya [] 

-- d. [ x+3 | (x,_) <- xys ]
addLeftPairWithThree :: [(Int, Int)] -> [Int]
addLeftPairWithThree xys = map (\(x,_) -> x+3) xys 
-- addLeftPairWithThree [(1,2),(2,3)] outputnya [4,5] 

-- e. [ x+4 | (x,y) <- xys, x+y < 5 ]
addLeftPairByFourIfSumLessThanFive :: [(Int, Int)] -> [Int]
addLeftPairByFourIfSumLessThanFive xys = map (\(x,y) -> x+4) (filter (\(x,y) -> x+y < 5) xys) 
-- addLeftPairByFourIfSumLessThanFive [(2,3),(4,2),(1,2)] outputnya [5] 

-- f. [ x+5 | Just x <- mxs ]
addFive mxs = map (\(Just x -> x+5)) (filter isJust mxs) 
-- addFive [Just 6, Just 10] outputnya [11,15] 

-- 2. Can you do it the other way around? I.e. rewrite the following expressions as list comprehensions.
-- a. map (+3) xs
addThree :: [Int] -> [Int]
addThree xs = [x+3 | x <- xs] 
-- addThree [1,2,3]

-- b. filter (>7) xs
moreThan7 :: [Int] -> [Int]
moreThan7 xs = [x | x <- xs, x>7]
-- moreThan7 [1,2,4,5,6,7,8]

-- c. concat (map (\x -> map (\y -> (x,y)) ys) xs)
concat :: [Int] -> [Int] -> [(Int, Int)]
concat xs ys = [(x,y) | x <- xs, y <- ys]
-- concat [1,2] [3,4]

-- d. filter (>3) (map (\(x,y) -> x+y) xys)
sumThenFilterMoreThanThree :: [(Int, Int)] -> [Int]
sumThenFilterMoreThanThree xys = [x+y | (x,y) <- xys, x+y > 3]
-- sumThenFilterMoreThanThree [(1,2),(2,3),(3,4)]
 