import Data.List

sumList [] = 0
sumList (x:xs) = x + sumList xs

lebihkecillazy n = [ x | x <- [1 .. ], x < n]

lebihkecil n = [ x | x <- [1 .. n]]

add [] []         = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs  =  1 : 1 : add fibs (tail fibs)


-- 1. Uraikan langkah evaluasi dari ekspresi berikut: [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]
[ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]
--cek satu-satu, untuk setiap x semua y cek memenuhi predikat x>y apa ga, kalo memenuhi dia jalanin x+y
-- (3 + 2) : [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]
-- (3 + 2) : (4 + 2) : (4 + 3) : [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]
-- (3 + 2) : (4 + 2) : (4 + 3) : []
-- [5,6,7]


-- 2. Buatlah fungsi divisor yang menerima sebuah bilangan bulat n dan mengembalikan
-- list bilangan bulat positif yang membagi habis n, Contoh:
-- LatihanLazy> divisor 12
-- [1,2,3,4,6,12]
divisor1 n = [x | x <- [1 .. n], n `mod` x == 0]
divisor2 n = [x | x <- [n, n-1, .. 1], n `mod` x == 0]

pembagi x n = (n `mod` x == 0) 
divisor n = [x | x <- [1 .. n], pembagi x n]


-- 3. Buatlah definisi quick sort menggunakan list comprehension.
-- ambil elemen pertama yaitu x sebagai pivot dan sort sisa list ke kanan dan kiri pivot
quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs,
                                  y <= x]
                   ++ [x] ++
                   quickSort [y | y <- xs,
                                  y > x]

quicksort1 :: (Ord a) => [a] -> [a]
quicksort1 [] = []
quicksort1 (x:xs) =
  let smallerSorted = quicksort1 [a | a <- xs, a <= x]
      biggerSorted = quicksort1 [a | a <- xs, a > x]
  in  smallerSorted ++ [x] ++ biggerSorted

-- 4. Buatlah definisi infinite list untuk permutation.
-- [2,3,4] - [3] = [2,4]
-- import Data.list
-- perm [1,2,3] = [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
x = [2,3,4] \\ [3]  

perm [] = [[]]
perm ls = [ x:sisa | x <- ls, sisa <- perm(ls\\[x])]

-- 5. Buatlah definisi untuk memberikan infinite list dari bilangan prima menerapkan
-- algoritma Sieve of Erastothenes.
primes = sieve [2 ..]
  where sieve (x:xs) = x : (sieve [y | y <- xs, y `mod` x /= 0])

primesR :: Integral a => a -> a -> [a]
primesR a b = takeWhile (<= b) $ dropWhile (< a) $ sieve2 [2..]
  where sieve2 (n:ns) = n:sieve2 [ m | m <- ns, m `mod` n /= 0 ]

-- 6. Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari element
-- triple bilangan bulat positif yang mengikut persamaan pythagoras x2 + y2 = z2
-- Contoh:
-- LatihanLazy > pythaTriple
-- [(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17),(12,16,20) … ]
-- Perhatian urutan penyusun list comprehension nya, coba mulai dari variable z!

-- Agar jarak integer z dan y mengecil
pythaTriple = [(x,y,z) |  z <- [5 ..]
                        , y <- [z, z-1 .. 1]
                        , x <- [y, y-1 .. 1]
                        , x*x + y*y == z*z ]

pythaTriple2 = [(x,y,z) |  z <- [5 ..]
                        , y <- [1 .. z]
                        , x <- [1 .. y]
                        , x^2 + y^2 == z^2 ]
