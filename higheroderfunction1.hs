-- I. Exercises from chapter 9-10 of The Craft of Functional Programming
import System.IO

-- 1. Define the length function using map and sum
lengthList :: [Int] -> Int
lengthList xs = sum (map (\x -> 1) xs)
-- lengthList [2,3,4] output 3

-- 2. What does map (+1) (map (+1) xs) do?
mapWhat :: [Int] -> [Int]
mapWhat xs = map (+1) (map (+1) xs)
-- mapWhat [1,2,3]
-- map f (map g xs)
-- jadi dia awalnya jalanin fungsi g yaitu (+1) jadi [2,3,4]
-- terus dia jalanin fungsi f yaitu (+1) jadi [3,4,5]

-- 3. Give the type of, and define the function iter so that iter n f x = f (f (... (f x)))
iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

-- 4. How would you define the sum of the squares of the natural numbers 1 to n using map and foldr?
sumSquares :: Integer -> Integer
sumSquares n = foldr (+) 0 (map (\x -> x*x) [1..n])
--                          (map (^2) [1..n])
-- sumSquares 7


-- 5. How does the function
-- mystery xs = foldr (++) [] (map sing xs)
-- where sing x = [x]
-- behave?
-- It accepts array xs, and return the array of each element xs in array.
-- Sing x wraps one element x into an array, x = [x].
-- map sing xs, wraps element array to one single array [1,2,3,4] = [[1],[2],[3],[4]]
-- foldr (++) [] folds right [] and merge every map sing xs together


-- 6. If id is the polymorphic identity function, defined by id x = x, explain the behavior
-- of the expressions (id . f) (f . id) (id f)
-- (id . f)  is the same as f, because the result of f is fed to the
--           identity function id, and thus not changed
-- (id :: Bool -> Bool)

-- (f . id)  is the same as f, because the argument of f is fed to the
--           identity function id, and thus not changed
-- (id :: Int -> Int)

-- id f      is the same as f, because applying id to something does not
--           change it
-- (id :: (Int -> Bool) -> (Int -> Bool))


-- 7. Define a function composeList which composes a list of functions into a single function.
-- You should give the type of composeList
composeList2 :: [a -> a] -> (a -> a)
composeList2 = foldr (.) id

composeList :: [a -> a] -> (a -> a)
composeList []     = id
composeList (f:fs) = f . composeList fs
-- composeList [(+1),(+2)] 2 should return 5


-- 8. (*) Define the function
-- flip :: (a -> b -> c) -> (b -> a -> c)
-- which reverses the order in which its function argument takes its arguments
flip :: (a -> b -> c) -> (b -> a -> c)  
flip f x y = f y x
simple n a b = n * (a - b)
newsimple = flip simple -- n a b = 3 4 5 jadi 4 3 5
-- newsimple flip 3 4 5
--            f   n a b
--            f   a n b (setelah diflip)
-- newsimple      4 3 5

-- mau balik a sama b
-- flipNew f x y z = f x z y 
-- flip (simple n)      n dikasih dulu jadi yg dibalik a sama b

-- function = flip (\a -> \b -> a/b)
-- function 1 2 output 2
