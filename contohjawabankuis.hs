-- Pembetulan kuis
-- 1. tentukan tipe dan buat definisi fungsi yang menerima tiga buah bilangan bulat positif
-- dan mengembalikan bilangan terbesar dari bilangan input tersebut
maxTiga :: Ord a => a -> a -> a -> a
maxTiga a b c = max (max a b) c

-- 4. buatlah definisi menggunakan foldl dan foldr untuk menjumlahkan elemen list
jumlahListMudah :: Num a => [a] -> a
jumlahListMudah xs = sum xs 

sumList [] = 0
sumList (x:xs) = x + sumList xs

-- lihat foldl di chapter 9
jumlahList :: [Integer] -> Integer
jumlahList xs = foldr (+) 0 xs

-- hitung banyak elemen list, chapter 5
lengthlist :: [a] -> Int
lengthlist [] = 0
lengthlist (x:xs) = 1 + lengthlist xs
-- length [2,3]
-- length (2:[3]) = 1 + length [3] = 1 + length (3,[]) = 1 + 1 + length [] = 2

-- || CATATAN: nulis tipe ada 2 cara, langsung tipenya sama inisialisasi tipenya ke variabel
-- || jumlahList :: [Integer] -> Integer
-- || jumlahListMudah :: Num a => [a] -> a 

-- 7. tentukan tipe dan definisi fungsi flip
flip :: (a -> b -> c) -> (b -> a -> c)  
flip f x y = f y x

-- 8. tentukan tipe dari fungsi UpdateState
> updateState :: (RobotState -> RobotState) -> Robot ()
> updateState u = Robot (\s _ _ -> return (u s, ()))
-- fungsi yang menerima 2 parameter _ robot state _ robot state dan mengembalikan robot() monad
-- tidak mengerti artinya u s, ()

-- 9. tentukan definisi fungsi turnRight
-- turn adalah fungsi yang menghasilkan robot Monad
> turnLeft  :: Robot ()
> turnLeft = updateState (\s -> s {facing = left (facing s)})

> turnRight :: Robot ()
> turnRight = updateState (\s -> s {facing = right (facing s)})
